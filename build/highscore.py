def generate_highscore(in_filename, out_filename):
	with open(in_filename, "rt") as f:
		scores = f.readlines()

	with open(out_filename, "wb") as f:
		for score in scores:
			name, points = score.split(',')
			points = int(points)
			data = bytearray((32 for _ in range(16)))
			data[0] = points
			data[1] = len(name)
			for i, char in enumerate(name):
				data[i + 2] = ord(char) - 97 + 1
			f.write(data)
				
