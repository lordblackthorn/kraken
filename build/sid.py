import subprocess
import os
import shlex

join = os.path.join

def create_tunes(song, output):
	cmd = f'gt2reloc {song} {output}.prg -D1 -WC0 -Z02'
	subprocess.run(shlex.split(cmd, posix=False))

def create_sound(instrument, output):
	cmd = f'ins2snd2 {instrument} {output} -b'
	subprocess.run(shlex.split(cmd, posix=False))