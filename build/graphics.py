import json

def generate_sprites(in_filename, out_filename):
	with open(in_filename, 'rb') as f:
		data = f.read()

	num_sprites = data[5]

	# Each sprite is 64byte long and the sprites start at byte 9
	sprites = bytearray(data[16: 16 + num_sprites * 64])

	with open(out_filename, "wb") as f:
		f.write(sprites)


def generate_charset(in_filename, out_filename):
	with open(in_filename, 'rb') as f:
		data = f.read()
	# Main charset data starts at byte 20 and is 2048 bytes long
	charset = data[20:2068]

	with open(out_filename, "wb") as f:
		f.write(charset)


def generate_pcharset(in_filename, out_filename):
	with open(in_filename, 'rb') as f:
		data = f.read()

	num = int(out_filename[-1]) - 1
	start = 20 + num * 2048
	charset = data[start:start + 114 * 8]

	with open(out_filename, "wb") as f:
		f.write(charset)


def generate_map(in_filename, out_filename):
	with open(in_filename, 'rb') as f:
		data = f.read()

	map_data = data[-960 * 2:][0::2]

	with open(out_filename, "wb") as f:
		f.write(map_data)


def generate_titlescreen(in_filename, out_filename):
	with open(in_filename, 'rb') as f:
		data = f.read()

	num_chars = 1024
	start_color_data = 20 + num_chars * 8
	map_data = data[-1000 * 2:][0::2]
	character_color = data[start_color_data: start_color_data + 1024]
	color_data = bytearray(1000)

	for i, character in enumerate(map_data):
		color_data[i] = character_color[character]

	with open(out_filename, "wb") as f:
		f.write(map_data)
		f.write(color_data)


def generate_bitmap(doodle_file, out_filename):
	with open(doodle_file, 'rb') as f:
		data = f.read()

	with open(out_filename, "wb") as f:
		f.write(data[2:])


def generate_petmate(petmate_file, out_filename):
	with open(petmate_file, 'rt') as f:
		petmate = json.load(f)
	last_screen = len(petmate['screens']) - 1
	frame_buff = petmate['framebufs'][last_screen]['framebuf']

	data = bytearray(40 * 25 * 2)
	index = 0
	for line in frame_buff:
		for char in line:
			data[index] = char['code']
			data[index + 1] = char['color']
			index += 2

	
	with open(out_filename, "wb") as f:
		f.write(data)
