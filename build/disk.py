import subprocess
import os
import shlex
import tempfile

join = os.path.join


def encode_rle(data):
	"""
	Compress a file using rle encoding. Uses 254 as escape character. 
	The algorithm to decompress the file is as following, assuming the decoder reads one byte at a time
	 - If the byte read is not 254, then the byte is file data
	 - If the byte is 254 then:
	   - If the next byte is 254, then the file data is 254
	   - Otherwise, the next byte is the counter and the third byte contains the file data. Counter can be 253 maximum

	The encoder can increase the total file size when there are many 254s, since each 254 requires 2 254s to store
	"""

	index = 0
	encoded = []
	while index != len(data):
		counter = 1
		byte = data[index]
		index += 1
		if byte == 254:
			encoded.append(254)
			encoded.append(254)
			continue
		# Try to eat as many bytes as possible
		while index < len(data) and counter < 253 and data[index] == byte:
			counter += 1
			index += 1
		#print(f'Byte {byte} is pencodedent {counter} times in a row')
		if counter > 2:
			encoded.append(254)
			encoded.append(counter)
			encoded.append(byte)
		else:
			for _ in range(counter):
				encoded.append(byte)
	return bytes(encoded)


def _build_c1541_args(disk):
	args = ' -format "' + disk['label'] + '" d64 ' + disk['name']
	for f in disk['files']:
		args += ' -write ' + f['fs_name'] + ' ' + f['d64_name']
		args += '' if not f['seq'] else ',s'

	return args

def _create_temp_files(disk):
	for f in disk['files']:
		file_name = f['fs_name']
		if 'rle' in f and f['rle'] and f['seq']:
			with open(file_name, 'rb') as handle:
				data_in = handle.read()
			data_out = encode_rle(data_in)
			temp_file = tempfile.NamedTemporaryFile(delete=False)
			temp_file.write(data_out)
			f['fs_name'] = temp_file.name
			temp_file.close()

def _delete_temp_files(disk):
	for f in disk['files']:
		if 'rle' in f and f['rle']:
			os.unlink(f['fs_name'])

def create(disks):
	for disk in disks:
		try:
			_create_temp_files(disk)
			cmd = f"c1541 {_build_c1541_args(disk)}"
			subprocess.run(shlex.split(cmd, posix=False))
		finally:
			_delete_temp_files(disk)
