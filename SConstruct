import build.disk

import build.graphics
import build.sid
import build.highscore
import os
import os.path

object_builder  = Builder(action='ca65 -g -t c64 -o $TARGET $SOURCE', 
	suffix='.o',
	src_suffix='.s')

def label_emitter(target, source, env):
	target.append(f'{str(target[0])}.label')
	return target, source

prg_builder = Builder(action='ld65 -Ln ${TARGET}.label -o $TARGET -C c64-asm.cfg --lib c64.lib -u __EXEHDR__ $SOURCES',
	suffix='.prg',
	src_suffix='.o',
	emitter=label_emitter)

def charset_builder_fun(target, source, env):
	build.graphics.generate_charset(str(source[0]), str(target[0]))

charset_builder = Builder(action=charset_builder_fun, suffix='.cs')

def pcharset_builder_fun(target, source, env):
	build.graphics.generate_pcharset(str(source[0]), str(target[0]))

pcharset_builder2 = Builder(action=pcharset_builder_fun, suffix='.cs2')
pcharset_builder3 = Builder(action=pcharset_builder_fun, suffix='.cs3')
pcharset_builder4 = Builder(action=pcharset_builder_fun, suffix='.cs4')

def map_builder_fun(target, source, env):
	build.graphics.generate_map(str(source[0]), str(target[0]))

map_builder = Builder(action=map_builder_fun, suffix='.map')

def titlescreen_builder_fun(target, source, env):
	build.graphics.generate_titlescreen(str(source[0]), str(target[0]))

titlescreen_builder = Builder(action=titlescreen_builder_fun, suffix='.colmap')

def sprite_builder_fun(target, source, env):
	build.graphics.generate_sprites(str(source[0]), str(target[0]))

sprite_builder = Builder(action=sprite_builder_fun, suffix='.spr')

def bitmap_builder_fun(target, source, env):
	build.graphics.generate_bitmap(str(source[0]), str(target[0]))

bitmap_builder = Builder(action=bitmap_builder_fun, suffix='.bitmap')

def petmate_builder_fun(target, source, env):
	build.graphics.generate_petmate(str(source[0]), str(target[0]))

petmate_builder = Builder(action=petmate_builder_fun, suffix='.petscii')

def sid_builder_fun(target, source, env):
	snd_file = str(target[0])
	build.sid.create_tunes(str(source[0]), snd_file)
	os.rename(f'{snd_file}.prg', snd_file)

sid_builder = Builder(action=sid_builder_fun, suffix='.tune')

def sound_builder_fun(target, source, env):
	snd_file = str(target[0])
	build.sid.create_sound(str(source[0]), snd_file)

sound_builder = Builder(action=sound_builder_fun, suffix='.snd')

def highscore_builder_fun(target, source, env):
	build.highscore.generate_highscore(str(source[0]), str(target[0]))

highscore_builder = Builder(action=highscore_builder_fun, suffix='.hs')

disks = [{'label': 'kraken,01', 'name': 'kraken.d64', 'files': [
	{'fs_name': 'kraken.prg', 'd64_name': 'kraken', 'seq': False},
	{'fs_name': 'data/kraken.cs', 'd64_name': 'charset', 'seq': True, 'rle': True},
	{'fs_name': 'data/kraken.cs2', 'd64_name': 'charset2', 'seq': True, 'rle': True},
	{'fs_name': 'data/kraken.cs3', 'd64_name': 'charset3', 'seq': True, 'rle': True},
	{'fs_name': 'data/kraken.cs4', 'd64_name': 'charset4', 'seq': True,  'rle': True},
	{'fs_name': 'data/kraken.spr', 'd64_name': 'sprite', 'seq': True, 'rle': True},
	{'fs_name': 'data/kraken.map', 'd64_name': 'map', 'seq': True, 'rle': True},
	{'fs_name': 'data/title_screen.colmap', 'd64_name': 'title', 'seq': True, 'rle': True},
	{'fs_name': 'data/kraken.petscii', 'd64_name': 'splash', 'seq': True},
	{'fs_name': 'data/kraken.tune', 'd64_name': 'music', 'seq': False},
	{'fs_name': 'data/highscore.hs', 'd64_name': 'highscore', 'seq': True},
	]}]

def disk_builder_fun(target, source, env):
	build.disk.create(disks)

import re
include = re.compile(r'\.(incbin|include)\s+"(.*)"')
def file_scan(node, env, path):
	content = node.get_text_contents()
	files = [node.File(i[1]) for i in include.findall(content)]
	return files
scan = Scanner(function = file_scan, skeys = ['.s'], recursive = True)

import os
env = Environment(BUILDERS={'c64Obj': object_builder,
				  'c64Prg': prg_builder, 
				  'charset': charset_builder,
				  'pcharset2': pcharset_builder2,
				  'pcharset3': pcharset_builder3,
				  'pcharset4': pcharset_builder4,
				  'map': map_builder,
				  'titlescreen': titlescreen_builder,
				  'sprite': sprite_builder,
				  'bitmap': bitmap_builder,
				  'petmate': petmate_builder,
				  'highscore': highscore_builder,
				  'sid': sid_builder,
				  'sound': sound_builder},
				  SCANNERS=scan,
				  ENV={'PATH' : os.environ['PATH']})
Export('env')

objs = SConscript('src/SConscript')
data_files = SConscript('data/SConscript')

prg = env.c64Prg('kraken', objs)
env.Default(prg)

disk_targets = [disk['name'] for disk in disks]
disk_sources = [file['fs_name'] for disk in disks for file in disk['files']]
env.Alias('disk', env.Command(disk_targets, disk_sources, disk_builder_fun))
