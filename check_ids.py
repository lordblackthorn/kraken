#!/usr/bin/python3
from glob import glob
import re

sources = glob("**/*.s") + glob("**/*.inc")

identifiers = []
symbols = []

patterns = [
	re.compile(".proc (\w+)"),
	re.compile(".macro (\w+)"),
	re.compile("(\w+) = "),
	re.compile("(\w+):"),
]

# Extract the identifiers and the global definitions
for source in sources:
	with open(source, "rt") as f:
		for line in f:
			if line.startswith('.global'):
				symbols += map(str.strip, line[7:].split(','))
			else:
				for pattern in patterns:
					match = pattern.match(line)
					if match:
						identifiers.append({'name': match.group(1), 'file': source, 'line': line, 'used': False})

# Check for usage
for source in sources:
	with open(source, "rt") as f:
		for line in f:
			if line.startswith('.global'):
				continue
			for identifier in identifiers:
				match = re.search(identifier['name'], line)
				if match and line != identifier['line']:
					identifier['used'] = True
for identifier in identifiers:
	if identifier['used'] is False:
		print(f"{identifier['name']} defined in {identifier['file']} is never used")

# Check if global symbols are declared but not defined
for symbol in symbols:
	found = False
	for identifier in identifiers:
		if identifier['name'] == symbol:
			found = True
			break
	if found is False:
		print(f"Global {symbol} is declared but not defined")
