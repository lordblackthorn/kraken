# Map
The metamap contains logical data related to the game map. It has the same size of the map and it resides at $400.
The idea is that given a tile in the map, the metadata is stored at the same position in the metamap.
Each tile is composed of four characters, so four bytes are available in the metadata per tile. There are two possible metadata structures, one used for walkable tiles, and one used for blocks.

## Walkable tile metadata:
 - If the first two bytes of the metadata are 0, then there is no metadata.
 - If the first two bytes are non zero, then they represent the address of a function to call. This is only used for forks. There is one function to handle forks on the border of the map (3 way forks and corners) and another one to handle the four way forks in the middle of the map.
  - Bytes 2 and 3 contain the parameters of the function. This is only valid for 3 way forks. The parameter in this case is an address that points to data containing the valid directions possible for that fork.
Only enemies use the function, the player is only interested in the fork data to know the valid directions.

## Blocks metadata
The first tile of the block contains data about the content of the block and the function to use to clear itself
 - The first two bytes represent the address of a function to call. There is a function for each block size
 - The third byte is 0 if the block hasn't been cleared, 1 otherwise. This is used to prevent the callback to be invoked more than once
 - The fourth byte contains the content id, i.e. whatever is inside the block

