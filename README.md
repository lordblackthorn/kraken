# Krakens

A game for C64. Released on [itch.io](https://cdr64.itch.io/krakens)

# Building
Building requires installing [scons](https://scons.org/) (Python 3 version) and the [cc65](https://www.cc65.org/) toolchain. [GoatTracker 2](https://sourceforge.net/projects/goattracker2/) executables are also required to be in the `PATH` environment variable.

## Build the prg
Run `scons` from the root directory

## Build the d64 disk image
Run `scons disk` from the root directory

# LICENSE
 * All code is released under the MIT license (see the `MIT_LICENSE.txt` file for copyright details)
 * All art, including music, sounds and graphics are copyright of Politopo [@politopo4](https://twitter.com/Politopo4). They are distributed and licensed under the Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
