.include "sid.inc"
.include "def.inc"

.code

; Preload A with the tune. Use #255 to stop music
.proc set_tune
	sta curr_tune
	cmp #$FF
	beq end

	jsr sound_addr

end:
	rts
.endproc

; Start playing a sound effect
; Preload A and Y with the sound effect address
.proc play_sound
	ldx #0
	jsr sound_addr + 6
	rts
.endproc

; Play a music frame
.proc play_music
	lda curr_tune
	cmp #$FF
	beq end
	jsr sound_addr + 3

end:
	rts
.endproc

.proc play_hit_sound
	lda #<hit_sound
	ldy #>hit_sound
	jsr play_sound
	rts
.endproc

.proc play_life_up_sound
	lda #<life_up_sound
	ldy #>life_up_sound
	jsr play_sound
	rts
.endproc

.proc play_pearl_sound
	lda #<pearl_sound
	ldy #>pearl_sound
	jsr play_sound
	rts
.endproc

.proc play_slow_down_sound
	lda #<slow_down_sound
	ldy #>slow_down_sound
	jsr play_sound
	rts
.endproc

.proc play_count_sound
	lda #<count_sound
	ldy #>count_sound
	jsr play_sound
	rts
.endproc

.data

curr_tune:
	.byte 255

hit_sound:
	.incbin "../data/ouch.snd"
pearl_sound:
	.incbin "../data/bling.snd"
count_sound:
	.incbin "../data/ca_ching.snd"
slow_down_sound:
	.incbin "../data/slow_down.snd"
life_up_sound:
	.incbin "../data/one_up.snd"
