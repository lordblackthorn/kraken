.include "util.inc"
.include "math.inc"
.include "walker.inc"
.include "ai.inc"

.code

.proc update_walkers
	; Skip walker #0 since that is the player
	load_zp $61, (walkers + .sizeof(Walker))

loop:
	ldy #Walker::active
	lda ($61), y
	beq next

	jsr is_walker_snapped
	bne no_fork

	jsr handle_speed
	jsr calc_walker_offset
	load_zp $FB, $400
	inc16 $FB, walker_offset
	ldy #1
	lda ($FB), y
	beq no_fork

	; If in a fork load the callback address in $6F/$70 and the parameter in $FD/$FE
	sta $70
	dey
	lda ($FB), y
	sta $6F
	ldy #40
	lda ($FB), y
	sta $FD
	iny
	lda ($FB), y
	sta $FE
	jsr callback ; invoke a fork_* function

no_fork:
	; Move the unit in the current direction
	jsr move_walker

next:
	; Prepare for the next loop
	lda #.sizeof(Walker)
	inc16 $61
	cmp_eq16_value $61, walkers_end
	bne loop

	rts
.endproc


; Double fast krakens speed if in line with the player
.proc handle_speed
	ldy #Walker::id
	lda ($61), y
	cmp #4
	bcs fast_kraken
	rts

fast_kraken:
	; Fast kranen, compare x and y with player
	ldy #Walker::x_p
	lda walkers, y
	sta temp
	lda ($61), y
	sta temp + 2
	iny
	lda walkers, y
	sta temp + 1
	lda ($61), y
	sta temp + 3
	cmp_eq16 temp, temp + 2
	beq in_line

	ldy #Walker::y_p
	lda walkers, y
	cmp ($61), y
	beq in_line

	; Set low speed
	ldy #Walker::speed
	lda #1
	sta ($61), y
	rts


in_line:
	; Set high speed
	ldy #Walker::speed
	lda #2
	sta ($61), y
	rts
.endproc

