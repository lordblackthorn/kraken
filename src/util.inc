.global memfill, memcopy, wait, temp, callback

.macro push8 addr
	lda addr
	pha
.endmac

.macro pop8 addr
	pla
	sta addr
.endmac

.macro push16 addr
	push8 addr
	push8 addr + 1
.endmac

.macro pop16 addr
	pop8 addr + 1
	pop8 addr
.endmac

; Compare numbers in addr1 vs addr2 and set the zero bit accordingly
; Only use this to check for equality
.macro cmp_eq16 addr1, addr2
	.local end
	lda addr1
	cmp addr2
	bne end
	lda addr1 + 1
	cmp addr2 + 1
end:
.endmac

; Compare numbers in addr vs immediate and set the zero bit accordingly
; Only use this to check for equality
.macro cmp_eq16_value addr, value
	.local end
	lda addr
	cmp #<value
	bne end
	lda addr + 1
	cmp #>value
end:
.endmac

; Compare number located in addr with 0 and set the zero bit accordingly
; Only use this to check for equality
.macro cmp16_with_0 addr
	.local end
	lda addr
	cmp #0
	bne end
	lda addr + 1
	cmp #0
end:
.endmac

; Store the symbol in addr. Normally useful to setup ZP addresses
.macro load_zp addr, symbol
	lda #<(symbol)
	sta addr
	lda #>(symbol)
	sta addr + 1
.endmac 
