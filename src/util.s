.code

.include "math.inc"
.include "util.inc"

; Copy blocks of 256 bytes from source to destination
; use $FB and $FC for source, $FD and $FE for destination
; preload X with the numbers of blocks to copy
.proc memcopy
	ldy #0
loop:  
	lda ($FB), y
	sta ($FD), y
	iny
	bne loop
	inc $FC
	inc $FE
	dex
	bne loop
	rts
.endproc

; Fill blocks of 256 bytes of memory with a specified value.
; Use $FD end $FE to store the starting address
; Preload X with the numbers of blocks to fill and A with the value
.proc memfill
	ldy #0
loop:  
	sta ($FD), y
	iny
	bne loop
	inc $FE
	dex
	bne loop
	rts
.endproc

; Waste time in a busy wait
; Preload a 16bit value in temp and temp+1 and this function will count downward until the value is 0
; Uses temp 
.proc wait
loop:
	dec16_by_1 temp
	cmp16_with_0 temp
	bne loop
	rts
.endproc

; Jmp to whatever is in $6F
.proc callback
	; routine in ($6F) must exit with a RTS
	jmp ($6F)
.endproc

.data

; Reserve a few bytes to be used as local variables
temp:
	.res 4

