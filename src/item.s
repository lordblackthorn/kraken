.include "item.inc"

.code

; Blitting functions prerequisites:
; Preload $FB with the address of the first top left character in the screen
; Preload $FD with the address of the first top left character in the color screen
; Preload Y with the offset (in characters offset) from $FB/$FD. 
.macro blit_item name, ch1, ch2, ch3, ch4, color
.proc .ident(.sprintf("blit_%s", name))
	lda #ch1
	sta ($FB), y
	lda #color
	sta ($FD), y
	iny
	sta ($FD), y
	lda #ch2
	sta ($FB), y

	; Move Y to the next line
	tya
	clc
	adc #39
	tay

	lda #ch3
	sta ($FB), y
	lda #color
	sta ($FD), y
	iny
	sta ($FD), y
	lda #ch4
	sta ($FB), y

	rts
.endproc
.endmacro

blit_item "pearl", 64, 65, 80, 81, 9
blit_item "key", 70, 71, 86, 87, 13
blit_item "gate", 76, 77, 92, 93, 9
blit_item "bubbles", 66, 67, 82, 83, 14
blit_item "algae", 78, 79, 94, 95, 13
blit_item "heart", 96, 97, 112, 113, 10

