.code

.include "input.inc"
.include "util.inc"

; Translate the row into an index (e.g. 11110111 -> 3)
; The index is stored in A. X is destroyed in the process
.proc matrix_to_index
	eor #$FF
	ldx #0
loop:
	lsr
	bcs end
	inx
	jmp loop
	
end:
	txa
	rts
.endproc

; Scan the keyboard and decode the key pressed
; Use the same keyboard matrix codes as the Kernal routine
.proc scan_keyboard
	sec
	ldx #0
	lda #%11111110

row_loop:
	sta $DC00
	ldy $DC01
	cpy #$FF
	bne decode

	; Next
	rol
	inx
	cpx #8
	bne row_loop

	; No key pressed
	lda #k_None
	jmp end

decode:
	; Matrix code = row_index * 8 + col_index
	txa
	asl
	asl
	asl
	sta mcode
	tya
	jsr matrix_to_index
	clc
	adc mcode
	cmp key_pressed
	beq end
	
	; Not the same as key pressed, so put the key in the buffer as well
	sta key_buffer

end:
	sta key_pressed
	rts
.endproc

; Return the last key pressed in A
; The key buffer is basically the last key pressed event. After calling this function, the event is consumed, and
; subsequent calls will return k_None (up until a new key is pressed).
; This is useful for debouncing, as the function returns a key press only once
.proc get_last_key
	lda key_buffer
	cmp #k_None
	beq end

	; If a key was read, clear the buffer
	ldx #k_None
	stx key_buffer

end:
	rts
.endproc

; Set A to 1 if the fire button was pressed. As with "get_last_key", this proc takes care of debouncing
.proc get_fire_key
	lda fire_buffer	
	beq end

	ldx #0
	stx fire_buffer

end:
	rts
.endproc

.proc scan_input
	lda #$FF
	sta $DC00
	lda $DC00
	tax ; Store the joystick status in X for backup
	cmp #$FF
	bne joystick_used

	stx joy_status
	jsr scan_keyboard
	rts

joystick_used:
	; If Fire is not pressed, nothing to do
	and #16
	bne end

	lda joy_status ; Previous status
	and #16
	beq end ; If it was already pressed, then nothing to do

	; If here, Fire has been pressed since the last scan_input, so we update the buffer
	lda #1
	sta fire_buffer

end:
	stx joy_status

	lda #k_None
	sta key_pressed

	rts
.endproc

; Wait for Fire or Space to be pressed
.proc wait_input
	load_zp temp, 5000
	jsr wait
	jsr get_last_key
	jsr get_fire_key
wait_key:
	jsr get_fire_key
	cmp #1
	beq end ; Fire pressed
	jsr get_last_key
	cmp #k_Space
	bne wait_key

end:
	rts
.endproc

.data

key_pressed:
	.res 1

mcode:
	.res 1

key_buffer:
	.byte k_None

joy_status:
	.res 1

fire_buffer:
	.byte 0

