.macro PString str
	.byte .strlen(str), str
.endmacro

.macpack cbm
.macro ScrString str
	.byte .strlen(str)
	scrcode str
.endmacro

; Helper macro to define a screen string
.macro def_sstring label, str
	.ident(.string(label)):
	ScrString str
.endmacro


.global print_string, print_byte, clear_text
