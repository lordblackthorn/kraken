.include "prompt.inc"
.include "input.inc"

.code

; Only support single line input. 1 character is used for the cursor
max_input_size = 14

; Prompt the user to type a string. End when hitting return.
; Preload $FB with the screen coordinates
; Result string is saved in input_string, in ScrString format
.proc prompt
	lda #0
	sta cursor_pos
	sta input_string
	tay
	lda cursor_char
	sta ($FB), y
loop:
	jsr get_last_key
	cmp #k_None
	beq loop
	cmp #k_Delete
	beq delete_last
	cmp #k_Return
	beq end

	ldy cursor_pos
	cpy #(max_input_size - 1)
	beq loop

	tax
	lda key_screen_map, x
	sta ($FB), y
	sta string_data, y

	iny
	sty cursor_pos
	lda cursor_char
	sta ($FB), y

	jmp loop

delete_last:
	; Delete cursor
	ldy cursor_pos
	beq loop

	lda #32
	sta ($FB), y

	; Replace the last letter with the cursor and save the new cursor pos
	dey
	lda cursor_char
	sta ($FB), y
	sty cursor_pos
	jmp loop

end:
	ldy cursor_pos
	sty input_string
	lda #32
	sta ($FB), y
	rts	
.endproc

.data

cursor_char:
	.byte 0

; Table to map a keycode to a screencode
; Unsupported/unprintable characters are mapped to space (32)
key_screen_map:
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 51 ; 3
	.byte 23 ; W
	.byte 1 ; A
	.byte 52 ; 4
	.byte 26 ; Z
	.byte 19 ; S
	.byte 5 ; E
	.byte 32
	.byte 53 ; 5
	.byte 18 ; R
	.byte 4 ; D
	.byte 54 ; 6
	.byte 3 ; C
	.byte 6 ; F
	.byte 20 ; T
	.byte 24 ; X
	.byte 55 ; 7
	.byte 25 ; Y
	.byte 7 ; G
	.byte 56 ; 8
	.byte 2 ; B
	.byte 8 ; H
	.byte 21 ; U
	.byte 22 ; V
	.byte 57 ; 9
	.byte 9 ; I
	.byte 10 ; J
	.byte 48 ; 0
	.byte 13 ; M
	.byte 11 ; K
	.byte 15 ; O
	.byte 14 ; N
	.byte 32 
	.byte 16 ; P
	.byte 12 ; L
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 32
	.byte 49 ; 1
	.byte 32
	.byte 32
	.byte 50 ; 2
	.byte 32
	.byte 32 ; Space
	.byte 17 ; Q
	.byte 32

.bss

cursor_pos:
	.res 1

input_string:
	.res 1

string_data:
	.res max_input_size

