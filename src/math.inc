.global to_bcd, bcd

; Increment a 16 bit number located at the specified address by either the amount in A or the 16 bit operand
; passed as second parameter
.macro inc16 addr, operand
	clc
	.ifnblank operand
		lda operand
	.endif
	adc addr
	sta addr
	lda addr + 1
	.ifnblank operand
		adc operand + 1
	.else
		adc #0
	.endif
	sta addr + 1
.endmac

; Increment a 16 bit number located at the specified address by 1
; Doesn't use registers
.macro inc16_by_1 addr
	.local end
	inc addr
	bne end

	inc addr + 1

end:
.endmac

; Multiply op1 by op2 and add the result to whatever is in addr.
; op1 must be a 8 bit value
; op2 must be a 16 bit value
; Values in op1 and op2 are destroyed during the operation
.macro mul16 addr, op1, op2
	.local loop, no_op
	ldx #8
loop:
	lsr op1
	bcc no_op
	inc16 addr, op2
no_op:
	asl op2
	rol op2 + 1
	dex
	bne loop
.endmac

; Decrement the value of the number in addr by the value addressed by the operand
; (in other words, execute addr -= operand)
.macro dec16 addr, operand
	sec
	lda addr
	sbc operand
	sta addr
	lda addr + 1
	sbc operand + 1
	sta addr + 1
.endmac

; Decrement a 16bit number located at the specified address by 1
.macro dec16_by_1 addr
	.local end
	dec addr
	lda #$FF
	cmp addr
	bne end
	dec addr + 1
end:
.endmac

; Shift the 16bit value addressed by addr to the right.
.macro shr16 addr
	lsr addr + 1
	ror addr
.endmac

; Shift the 16bit value addressed by addr to the left
.macro shl16 addr
	asl addr
	rol addr + 1
.endmac

; Shift A left X times
.macro shl
	.local loop
	.local end
	cpx #0
	beq end
loop:
	asl
	dex
	bne loop
end:
.endmac
