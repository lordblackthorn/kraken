.include "math.inc"
.import temp

.code

; Convert the number in temp to its BCD representation
; Store the result in bcd
.proc to_bcd
	lda #0
	sta bcd
	sta bcd + 1
	sed
	ldx #8
loop:
	asl temp
	lda bcd
	adc bcd
	sta bcd
	lda bcd + 1
	adc bcd + 1
	sta bcd + 1
	dex
	bne loop

	cld
	rts
.endproc

.data

bcd:
	.res 2