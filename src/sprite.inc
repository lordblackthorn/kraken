; The actual implementation of the functions is in 'sprite.s', but including this file
; will import all the needed symbols to work with sprites
; Sprite ID: a number between 0 and 7, it's the sprite to use
; Sprite number: the index of the sprite. This number is added to 'sprite_ptr_base' to set the sprite pointer
; Sprite number has been designed so that 0 is the first sprite in the binary file containg all the sprites
; set_active_sprite must be called to to set the active sprite, before calling sprite related functions.

.macro set_active_sprite id
	.if id = 0
		lda #1
	.elseif id = 1
		lda #2
	.elseif id = 2
		lda #4
	.elseif id = 3
		lda #8
	.elseif id = 4
		lda #16
	.elseif id = 5
		lda #32
	.elseif id = 6
		lda #64
	.elseif id = 7
		lda #128
	.endif
	sta spr_mask

	lda #id
	sta spr_active
.endmac

.global enable_sprite, set_sprite_color
.global set_sprite_x, set_sprite_y
.global spr_x, spr_y, spr_mask, spr_active

