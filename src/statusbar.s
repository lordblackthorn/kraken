.include "statusbar.inc"
.include "def.inc"
.include "string.inc"
.include "util.inc"
.include "math.inc"
.include "stage.inc"

.code

; Update the statusbar
.proc update_statusbar
	; Print lives
	load_zp $65, lives_str
	load_zp $FB, screen + 960
	lda #40
	sta temp
	jsr print_string
	lda #7
	inc16 $FB
	lda lives
	sta temp
	jsr to_bcd
	jsr print_byte

	; Print stage
	load_zp $65, stage_str
	lda #9
	inc16 $FB
	lda #0
	sta temp
	jsr print_string
	lda #7
	inc16 $FB
	lda curr_stage
	sta temp
	inc temp
	jsr to_bcd
	jsr print_byte
	rts
.endproc

.data
def_sstring lives_str, "lives: "
def_sstring stage_str, "stage: "


.bss
lives:
	.res 1
