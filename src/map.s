.include "map.inc"
.include "util.inc"
.include "math.inc"
.include "walker.inc"
.include "rand.inc"
.include "def.inc"
.include "stage.inc"
.include "item.inc"
.include "statusbar.inc"
.include "sid.inc"

.code

def_randrange 2
def_randrange 3

.macro init_fork addr, type, data
	load_zp $FD, addr
	ldy #0
	lda #<type
	sta ($FD), y
	iny
	lda #>type
	sta ($FD), y
	.ifnblank data
	ldy #40
	lda #<data
	sta ($FD), y
	iny
	lda #>data
	sta ($FD), y
	.endif
.endmac

.macro setup_block addr, fun
	ldy #82
	lda #<fun
	sta addr + $400, y
	iny
	lda #>fun
	sta addr + $400, y
.endmacro

; Fill up the meta map with fork data
.proc init_meta_map
	load_zp $FD, $400
	ldx #4
	lda #0
	jsr memfill

	; Corners
	init_fork $400, fork_3, top_left_corner
	init_fork $426, fork_3, top_right_corner
	init_fork $770, fork_3, bottom_left_corner
	init_fork $796, fork_3, bottom_right_corner

	; Top
	init_fork $408, fork_3, top_fork
	init_fork $410, fork_3, top_fork
	init_fork $416, fork_3, top_fork
	init_fork $41E, fork_3, top_fork

	; Bottom
	init_fork $778, fork_3, bottom_fork
	init_fork $780, fork_3, bottom_fork
	init_fork $786, fork_3, bottom_fork
	init_fork $78E, fork_3, bottom_fork

	; Left
	init_fork $4F0, fork_3, left_fork
	init_fork $5E0, fork_3, left_fork
	init_fork $6D0, fork_3, left_fork

	; Right
	init_fork $516, fork_3, right_fork
	init_fork $606, fork_3, right_fork
	init_fork $6F6, fork_3, right_fork

	; 4-way forks
	init_fork $4F8, fork_4
	init_fork $500, fork_4
	init_fork $506, fork_4
	init_fork $50E, fork_4
	init_fork $5E8, fork_4
	init_fork $5F0, fork_4
	init_fork $5F6, fork_4
	init_fork $5FE, fork_4
	init_fork $6D8, fork_4
	init_fork $6E0, fork_4
	init_fork $6E6, fork_4
	init_fork $6EE, fork_4

	; Init block data
	setup_block 0, fun_block_type_1
	setup_block 8, fun_block_type_1
	setup_block 16, fun_block_type_2
	setup_block 22, fun_block_type_1
	setup_block 30, fun_block_type_1
	setup_block 240, fun_block_type_1
	setup_block 248, fun_block_type_1
	setup_block 256, fun_block_type_2
	setup_block 262, fun_block_type_1
	setup_block 270, fun_block_type_1
	setup_block 480, fun_block_type_1
	setup_block 488, fun_block_type_1
	setup_block 496, fun_block_type_2
	setup_block 502, fun_block_type_1
	setup_block 510, fun_block_type_1
	setup_block 720, fun_block_type_3
	setup_block 728, fun_block_type_3
	setup_block 736, fun_block_type_4
	setup_block 742, fun_block_type_3
	setup_block 750, fun_block_type_3

	rts
.endproc

; Draw an item in the block. Same prerequisites of fun_block_type_*, but Y
; must contain the offset of the first character of the item from the first character of the block
.proc fill_block_with_item
	sty temp + 3
	load_zp $FB, screen
	load_zp $FD, $D800
	inc16 $FB, temp
	inc16 $FD, temp

	ldy temp + 3
	lda temp + 2
	cmp #0 ; Pearl
	beq pearl
	cmp #1 ; Key
	beq key
	cmp #2 ; Algae
	beq algae
	
	; Case #3: Heart
	jsr blit_heart
	jsr play_life_up_sound
	inc lives
	jsr update_statusbar

	rts

pearl:
	jsr blit_pearl
	jsr play_pearl_sound
	inc num_pearls
	rts

algae:
	jsr blit_algae
	jsr play_slow_down_sound
	lda #1
	sta algae_hit
	rts

key:
	jsr blit_key
	jsr play_pearl_sound
	lda #1
	sta key_taken

	; Make the gate appear
	lda stage_gate_x
	sta offset
	lda stage_gate_x + 1
	sta offset + 1
	.repeat 3
	shr16 offset
	.endrep
	lda stage_gate_y
	.repeat 3
	lsr
	.endrep
	sta temp + 1
	lda #0
	sta temp + 2
	lda #40
	sta temp
	mul16 offset, temp, temp + 1
	load_zp $FB, screen
	load_zp $FD, $D800
	inc16 $FB, offset
	inc16 $FD, offset
	ldy #0
	jsr blit_gate

	rts
.endproc

; These functions are invoked the first time a block is cleared.
; There is one function for each block size
; The prerequisites are:
;  - 'temp + 0/1' must be filled with the screen offset of the top left character of the block.
;  - 'temp + 2' must contain the content id

.proc fun_block_type_1
	ldy #42
	jsr fill_block_with_item
	rts
.endproc

.proc fun_block_type_2
	ldy #41
	jsr fill_block_with_item
	rts
.endproc

.proc fun_block_type_3
	ldy #2
	jsr fill_block_with_item
	rts
.endproc

.proc fun_block_type_4
	ldy #1
	jsr fill_block_with_item
	rts
.endproc

; For enemies, choose a random new direction in a 4 way fork
; For the player, move the walker
; Preload $61 with the walker address
.proc fork_4
	cmp_eq16_value $61, walkers
	beq player

	; Enemy
	jsr rand_3
	; Avoid going back to the same direction the walker is coming from.
	; To achieve this, add #2 to select the opposite direction and add 1-3 to it
	sta temp
	inc temp
	ldy	#Walker::direction
	lda ($61), y
	clc
	adc #2
	adc temp
	and #$03 ; Modulo 4
	sta ($61), y
	rts

player:
	lda new_player_direction
	ldy	#Walker::direction
	sta walkers, y
	jsr move_walker
	rts
.endproc

; For enemies, choose a random new direction in a 3 way fork (or a corner)
; For the player check if the wanted direction is valid AND move the walker
; Preload $61 with the walker address and $FD with the fork data
.proc fork_3
	cmp_eq16_value $61, walkers
	beq player

	; Enemy
	jsr rand_2
	sta temp
	ldy	#Walker::direction
	lda ($61), y
	asl
	clc
	adc temp
	tay
	lda ($FD), y
	ldy	#Walker::direction
	sta ($61), y
	rts

player:
	; Check if the player wants to move in a valid direction
	ldy #0
	lda ($FD), y
	cmp new_player_direction
	beq valid_direction
	iny
	lda ($FD), y
	cmp new_player_direction
	beq valid_direction
	rts

valid_direction:
	ldy #Walker::direction
	lda new_player_direction
	sta walkers, y

	jsr move_walker
	rts
.endproc

.data
; Each table down here has 4 rows and two cols.
; The row is the current direction of the walker and the cols are the possible new directions (to be chosen randomly)

; Forks at the top of the screen (no corners)
top_fork:
	.byte 1, 3 ; Coming from south
	.byte 1, 2 ; Coming from left
	.byte 255, 255 ; Not possible
	.byte 2, 3 ; Coming from right

; Forks at the bottom of the screen (no corners)
bottom_fork:
	.byte 0, 0 ; Only possible at the start of the stage (always go up)
	.byte 0, 1 ; Coming from left
	.byte 1, 3 ; Coming from north
	.byte 0, 3 ; Coming from right

; Forks at the right of the screen (no corners)
right_fork:
	.byte 0, 3 ; Coming from south
	.byte 0, 2 ; Coming from left
	.byte 2, 3 ; Coming from north
	.byte 255, 255 ; Not possible

; Forks at the left of the screen (no corners)
left_fork:
	.byte 0, 1 ; Coming from south
	.byte 255, 255 ; Not possible
	.byte 1, 2 ; Coming from north
	.byte 0, 2 ; Not possible

; Corners
top_left_corner:
	.byte 1, 2 ; Coming from south
	.byte 255, 255 ; Not possible
	.byte 255, 255 ; Not possible
	.byte 1, 2 ; Coming from right

top_right_corner:
	.byte 2, 3 ; Coming from south
	.byte 2, 3 ; Coming from left
	.byte 255, 255 ; Not possible
	.byte 255, 255 ; Not possible

bottom_right_corner:
	.byte 0, 3 ; Only possible at the start of the stage
	.byte 0, 3 ; Coming from left
	.byte 0, 3 ; Coming from north
	.byte 255, 255 ; Not possible

bottom_left_corner:
	.byte 0, 1 ; Only possible at the start of the stage
	.byte 255, 255 ; Not possible
	.byte 0, 1 ; Coming from north
	.byte 0, 1 ; Coming from right

; These are the walking spaces around blocks, used to test if blocks have been "cleared".

; type 1 are 3x2 blocks
block_type_1:
	.byte 0, 2, 4, 6, 8, 80, 88, 160, 168, 240, 242, 244, 246, 248 ; 14

; type 2 are 2x2 blocks
block_type_2:
	.byte 0, 2, 4, 6, 80, 86, 160, 166, 240, 242, 244, 246 ; 12

; type 3 are 3x1 blocks
block_type_3:
	.byte 0, 2, 4, 6, 8, 80, 88, 160, 162, 164, 166, 168 ; 12

; type 4 are 2x1 blocks
block_type_4:
	.byte 0, 2, 4, 6, 80, 86, 160, 162, 164, 166 ; 10


; Addr is the offset of upper left corner outside the block
; Type is one of the block types
; Size is the size of each block type
.macro check_block addr, type, size
.local loop
.local end
	lda addr + $400 + 122
	bne end ; Block already cleared
	ldx #0
loop:
	lda type, x
	tay
	lda screen + addr, y
	cmp #66 ; The "steps" tile
	bne end
	inx
	cpx #size
	bne loop

	; Mark the block as cleared and invoke the block callback
	lda addr + $400 + 82
	sta $6F
	lda addr + $400 + 83
	sta $70
	load_zp temp, addr + 82
	lda #1
	sta addr + $400 + 122
	lda addr + $400 + 123
	sta temp + 2
	jsr callback

end:
.endmac

.proc check_blocks
	check_block 0, block_type_1, 14
	check_block 8, block_type_1, 14
	check_block 16, block_type_2, 12
	check_block 22, block_type_1, 14
	check_block 30, block_type_1, 14
	check_block 240, block_type_1, 14
	check_block 248, block_type_1, 14
	check_block 256, block_type_2, 12
	check_block 262, block_type_1, 14
	check_block 270, block_type_1, 14
	check_block 480, block_type_1, 14
	check_block 488, block_type_1, 14
	check_block 496, block_type_2, 12
	check_block 502, block_type_1, 14
	check_block 510, block_type_1, 14
	check_block 720, block_type_3, 12
	check_block 728, block_type_3, 12
	check_block 736, block_type_4, 10
	check_block 742, block_type_3, 12
	check_block 750, block_type_3, 12
	rts
.endproc

.bss

new_player_direction:
	.res 1

offset:
	.res 2
