.include "math.inc"
.include "util.inc"
.include "disk.inc"

.code

; Load a rle encoded file in memory
; Preload $FD with the destination address
; Preload $FB with the address containing the file name (Pascal string)
; After the call, 'file_status' contains 1 if the file was successfully loaded, 0 otherwise
.proc load_file_rle
	jsr open_file_r

loop:
	inc $D020
	jsr read_byte
	ldx file_status
	beq end ; Error
	cpx #1 ; EOF
	beq end

	cmp #254
	bne append

	; If the byte read is 254, then read again
	jsr read_byte
	ldx file_status
	beq end ; Error
	cmp #254
	beq append ; 2 254s read, append 254

	; If here, then we just read the counter, now read the data and append counter times
	sta temp ; Store the counter
	jsr read_byte
	ldx file_status
	beq end ; Error
	ldy temp
	dey
inflate:
	sta ($FD), y
	dey
	bne inflate
	sta ($FD), y ; Store for y = 0
	lda temp
	inc16 $FD
	jmp loop

append:
	ldy #0
	sta ($FD), y
	inc16_by_1 $FD
	jmp loop

end:
	jsr close_file
	rts
.endproc

; Load a file in memory, assuming the file was RLE encoded
; Preload $FD with the destination address
; Preload $FB with the address containing the file name (Pascal string)
; After the call, 'file_status' contains 1 if the file was successfully loaded, 0 otherwise
.proc load_file
	jsr open_file_r
	lda #255
	sta temp
	sta temp + 1
	jsr read_file_chunk
	jsr close_file
	rts
.endproc

; Load a prg file at the address specified by the first two bytes of the file
; Preload $FB with the address containing the file name (Pascal string)
; After the call, 'file_status' contains 1 if the file was successfully loaded, 0 otherwise
.proc load_prg
	lda #1
	sta file_status

	ldy #0
	lda ($FB), y
	inc16_by_1 $FB
	ldx $FB
	ldy $FC
	jsr $FFBD ; call SETNAM

 	lda #1 ; file number
	ldx #8 ; device 8
	ldy #1 ; use address stored in file
	jsr $FFBA ; call SETLFS

	lda #0 ; load (not verify)
	jsr $FFD5 ; call LOAD
	bcc end ; if carry is clear, no error

	lda #0
	sta file_status

end:
	rts
.endproc

.proc open_file
	ldy #0
	lda ($FB), y
	inc16_by_1 $FB
	ldx $FB
	ldy $FC
	jsr $FFBD ; call SETNAM

 	lda #2 ; file number
	ldx #8 ; device 8
	ldy #2 ; channel
	jsr $FFBA ; call SETLFS

	jsr $FFC0 ; call OPEN

	rts
.endproc

; Open a file for reading
; Preload $FB with the address containing the file name (Pascal string)
.proc open_file_r
	jsr open_file
	ldx #2
	jsr $FFC6 ; call CHKIN (switch input to file 2)
	rts
.endproc

; Read a byte from the current open file and return it in A
; Set the file_status with the following values:
; 0 - Error
; 1 - EOF reached, Could not read a byte
; 2 - Byte read
.proc read_byte
	lda #2
	sta file_status

	jsr $FFB7 ; call READST (read status byte)
	bne eof
	jsr $FFCF ; call CHRIN (read a byte)
	rts

eof:
	and #$BF ; Check for causes other than EOF
	beq end

	lda #0
	sta file_status
	rts

end: ; Actual eof
	lda #1
	sta file_status
	rts
.endproc

; Read a specified number of bytes from a previously open file (or less if EOF or an error is detected)
; Preload $FD with the destination address
; Preload 'temp' and 'temp + 1' with the number of bytes to read
; FD gets updated during this call and points to the next free byte.
; This means that subsequent calls to read_file_r can be made without having to update FD between calls
; After the call, 'file_status' contains 1 if some bytes were read and EOF was reached,
; 0 in case of error and 2 if all bytes were read and EOF was not reached
.proc read_file_chunk
	lda #0
	sta temp + 2
	sta temp + 3
loop:
	inc $D020
	jsr read_byte
	ldx file_status
	beq end ; Error
	cpx #1 ; EOF
	beq end

	ldy #0
	sta ($FD), y
	inc16_by_1 $FD
	inc16_by_1 temp + 2
	cmp_eq16 temp, temp + 2
	bne loop

end:
	rts
.endproc

.proc close_file
	lda #2
	jsr $FFC3 ; call CLOSE
	jsr $FFCC ; call CLRCHN
	rts
.endproc

; Open a file for writing
; Preload $FB with the address containing the file name (Pascal string)
; Preload $AE with the address containing the data to save
; Preload temp with the number of bytes to save
.proc save_file
	jsr open_file
	ldx #2
	jsr $FFC9 ; call CHKOUT (switch input to file 2)

loop:
	ldy #0
	lda ($AE), y
	jsr $FFD2 ; call CHROUT
	inc16_by_1 $AE
	dec temp
	bne loop

	jsr close_file

	rts
.endproc

.data

file_status:
	.res 1
