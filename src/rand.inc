; Define a function that returns values between 0 and max - 1
.macro def_randrange max
.proc .ident(.sprintf("rand_%d", max))
jsr rand
.repeat max - 1, i
cmp #((1 + 255 / max) * (i + 1))
bcc .ident(.sprintf("return_%d", i))
.endrepeat
lda #(max - 1)
rts

.repeat max - 1, i
.ident(.sprintf("return_%d", i)):
	lda #i
	rts
.endrepeat
.endproc
.endmac

.global rand, seed
