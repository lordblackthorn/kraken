; VIC-II location, as seen by the CPU
vicii = $4000

; screen location, as seen by the CPU
screen = vicii

; charsets location, as seen by the CPU
charset_addr = vicii + 2048
charset2_addr = vicii + 4096
charset3_addr = vicii + 6144
charset4_addr = vicii + 8192

; sprites vector location, as seen by the VIC-II
; Must be divisible by 64
sprite_vicii_addr = 10240 ; After charset and screen

; first sprite pointer
sprite_ptr_base = sprite_vicii_addr / 64

; sprites vector location, as seen by the CPU
sprite_addr = vicii + sprite_vicii_addr

sound_addr = $C000

; Data mapping
screen_bak = $8000
titlescreen = $8400
titlecolor = $8400 + 1000