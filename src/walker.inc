.struct Walker
	x_p .word ; screen h. position
	y_p .byte ; screen v. position
	speed .byte ; Movement increment/decrement in pixels. Powers of two only
	id .byte ; sprite used. 0 is the player
	sprite_mask .byte ; 2^id
	direction .byte ; 0 - up, 1 - right, 2 - down, 3 - left
	active .byte ; 1 if active, 0 otherwise
.endstruct

player_sprite = 32
fast_kraken_sprite = 16

.define walker_addr walkers + i * .sizeof(Walker)
.define max_walkers 6

.global init_walkers, move_walker, is_walker_snapped
.global calc_walker_offset, walker_offset
.global walkers, walkers_end
