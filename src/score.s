.include "score.inc"
.include "stage.inc"
.include "util.inc"
.include "def.inc"
.include "string.inc"
.include "math.inc"
.include "input.inc"
.include "screen.inc"
.include "sid.inc"

.code

.proc show_score_screen
	jsr clear_text_screen

	jsr print_stage_completed
	jsr calculate_points

	load_zp $FB, screen + 640 + 4
	lda #0
	sta temp
	load_zp $65, str_presskey
	jsr print_string

	jsr wait_input
	rts
.endproc

.proc show_gameover_screen
	lda #3
	jsr set_tune

	jsr clear_text_screen

	; Print game over
	load_zp $FB, screen + 240 + 15
	lda #0
	sta temp
	load_zp $65, str_gameover
	jsr print_string

	jsr calculate_points

	load_zp $FB, screen + 640 + 4
	lda #0
	sta temp
	load_zp $65, str_presskey
	jsr print_string

	jsr wait_input

	lda #0
	jsr set_tune

	rts
.endproc

.proc show_victory_screen
	jsr clear_text_screen

	; Print congratulations
	load_zp $FB, screen + 240 + 12
	lda #0
	sta temp
	load_zp $65, str_congrats
	jsr print_string

	load_zp $FB, screen + 320 + 12
	lda #0
	sta temp
	load_zp $65, str_gamebeat
	jsr print_string

	load_zp temp, 10000
	jsr wait

	jsr calculate_points

	load_zp $FB, screen + 640 + 4
	lda #0
	sta temp
	load_zp $65, str_presskey
	jsr print_string

	jsr wait_input

	rts
.endproc

.proc print_stage_completed
	load_zp $FB, screen + 252
	lda #0
	sta temp
	load_zp $65, str_stage
	jsr print_string
	tya
	inc16 $FB

	lda curr_stage
	sta temp
	jsr to_bcd
	jsr print_byte
	tya
	inc16 $FB

	lda #0
	sta temp
	load_zp $65, str_completed
	jsr print_string

	rts
.endproc

.proc calculate_points
loop:
	jsr play_count_sound
	load_zp temp, 8000
	jsr wait

	load_zp $FB, screen + 440 + 5
	lda #35
	sta temp
	load_zp $65, str_pearls
	jsr print_string
	lda str_pearls
	inc16 $FB

	lda num_pearls
	sta temp
	jsr to_bcd
	jsr print_byte

	load_zp $FB, screen + 440 + 25
	lda score
	sta temp
	jsr to_bcd
	jsr print_byte
	tya
	inc16 $FB

	lda #0
	sta temp
	load_zp $65, str_points
	jsr print_string

	lda num_pearls
	beq end
	dec num_pearls
	inc score
	jmp loop

end:
	rts
.endproc

.data
def_sstring str_completed, " completed"
def_sstring str_stage, "stage "
def_sstring str_pearls, "pearls:"
def_sstring str_points, " points"
def_sstring str_presskey, "press space or fire to continue"
def_sstring str_gameover, "game over"
def_sstring str_congrats, "congratulations"
def_sstring str_gamebeat, "you beat krakens"

.bss
score:
	.res 1
