.macro save_registers
	pha
	txa
	pha
	tya
	pha
.endmacro

.macro restore_registers
	pla
	tay
	pla
	tax
	pla
.endmacro

.global setup_interrupt, disable_raster_interrupt, enable_kernal

