.include "walker.inc"
.include "util.inc"
.include "math.inc"
.include "sprite.inc"
.include "def.inc"

.code

; Set the zero flag bit if the walker is snapped to grid
; Preload $61 with the walker id
.proc is_walker_snapped
	ldy #Walker::y_p
	lda ($61), y
	and #$0F
	bne end

	ldy #Walker::x_p
	lda ($61), y
	and #$0F

end:
	rts
.endproc

.proc calc_walker_offset
	; y_p and x_p are pixel coordinates; they must be converted to character coords by dividing them by 8
	; offset = int(x_p / 8) + int(y_p / 8) * 40

	ldy #Walker::x_p
	lda ($61), y
	sta walker_offset
	iny
	lda ($61), y
	sta walker_offset + 1
	.repeat 3
	shr16 walker_offset
	.endrep

	ldy #Walker::y_p
	lda ($61), y
	.repeat 3
	lsr
	.endrep
	sta temp + 1
	lda #0
	sta temp + 2

	lda #40
	sta temp

	mul16 walker_offset, temp, temp + 1

	rts
.endproc

.proc move_walker_up
	ldy #Walker::y_p
	lda ($61), y
	sec
	sbc speed
	sta ($61), y
	sta spr_y
	jsr set_sprite_y
	rts
.endproc

.proc move_walker_down
	ldy #Walker::y_p
	lda ($61), y
	clc
	adc speed
	sta ($61), y
	sta spr_y
	jsr set_sprite_y
	rts
.endproc

.proc move_walker_left
	ldy #Walker::x_p
	lda ($61), y
	sta spr_x
	iny
	lda ($61), y
	sta spr_x + 1

	dec16 spr_x, speed

	lda spr_x
	ldy #Walker::x_p
	sta ($61), y
	iny
	lda spr_x + 1
	sta ($61), y
	jsr set_sprite_x
	rts
.endproc

.proc move_walker_right
	ldy #Walker::x_p
	lda ($61), y
	sta spr_x
	iny
	lda ($61), y
	sta spr_x + 1

	lda speed
	inc16 spr_x

	lda spr_x
	ldy #Walker::x_p
	sta ($61), y
	iny
	lda spr_x + 1
	sta ($61), y
	jsr set_sprite_x
	rts
.endproc

; Move a walker according to the current direction
; Preload $61 with the walker address
.proc move_walker
	ldy #Walker::id
	lda ($61), y
	sta spr_active
	ldy #Walker::sprite_mask
	lda ($61), y
	sta spr_mask

	ldy #Walker::speed
	lda ($61), y
	sta speed

	ldy #Walker::direction
	lda ($61), y
	cmp #0
	beq move_up
	cmp #1
	beq move_right
	cmp #2
	beq move_down
	cmp #3
	beq move_left

	rts
	
move_up:
	jsr move_walker_up
	rts

move_down:
	jsr move_walker_down
	rts

move_left:
	jsr move_walker_left
	rts

move_right:
	jsr move_walker_right
	rts

.endproc

.data

; Current walker speed. Has to be 16bit because dec16 expects a 16bit operand
speed:
	.byte 0, 0

.bss

walkers:
	.repeat max_walkers
	.tag Walker
	.endrep
walkers_end:

walker_offset:
	.res 2
