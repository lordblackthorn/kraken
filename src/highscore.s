.include "highscore.inc"
.include "math.inc"
.include "util.inc"
.include "score.inc"
.include "def.inc"
.include "screen.inc"
.include "string.inc"
.include "prompt.inc"
.include "disk.inc"
.include "input.inc"
.include "interrupt.inc"
.include "sid.inc"

.code

.proc show_highscore
	load_zp $FB, screen + 160 + 15
	lda #0
	sta temp
	load_zp $65, str_highscore
	jsr print_string

	lda #0
	sta counter
loop:
	; Point $FB at the beginning of the row
	load_zp $FB, screen + 280
	lda counter
	sta temp
	load_zp temp + 1, 40
	mul16 $FB, temp, temp + 1

	; Print the row number
	lda counter
	sta temp
	inc temp
	jsr to_bcd
	jsr print_byte

	; Print the name
	lda #7
	inc16 $FB
	load_zp $65, highscore_table
	lda counter
	.repeat 4
	asl
	.endrep
	inc16 $65
	inc16_by_1 $65
	lda #0
	sta temp
	jsr print_string

	; Print the score
	lda #25
	inc16 $FB
	load_zp $65, highscore_table
	lda counter
	.repeat 4
	asl
	.endrep
	inc16 $65
	ldy #0
	lda ($65), y
	sta temp
	jsr to_bcd
	jsr print_byte

	inc counter
	lda #10
	cmp counter
	beq end
	jmp loop

end:
	rts
.endproc

; Update the score of the current line and clear the name
; Preload X with the offset from the start of the highscore table in bytes
.proc clear_highscore_row
	lda score
	sta highscore_table, x
	lda #0
	inx
	sta highscore_table, x
	rts
.endproc

.proc prompt_name
	load_zp temp, 5000
	jsr wait
	jsr get_last_key

	; Point $FB at the prompt point
	load_zp $FB, screen + 280
	lda counter
	sta temp
	load_zp temp + 1, 40
	mul16 $FB, temp, temp + 1
	lda #7
	inc16 $FB
	jsr prompt
	
	; Copy the string to the table
	lda counter
	.repeat 4
	asl
	.endrep
	tax
	inx
	ldy #0
loop:
	lda input_string, y
	sta highscore_table, x
	inx
	iny
	cpy #14
	bne loop

	rts
.endproc

; Update the highscore list
; Preset 'score' with the new score
.proc update_highscore
	lda #0
	jsr set_tune
	jsr clear_text_screen
	
	; Find the point in the table where to add the new score
	lda #0
	sta counter
loop:
	lda counter
	.repeat 4
	asl
	.endrep
	tax
	lda highscore_table, x
	cmp score
	bcs next ; highscore >= score, go to next highscore

	; highscore < score, insert the record here
	; Step 1: make room by scrolling the records after temp down
	jsr scroll_down
	; Step 2: Clear the line and show the table
	jsr clear_highscore_row
	push8 counter
	jsr show_highscore
	pop8 counter
	; Step 3: Prompt for a new name
	jsr prompt_name
	; Step 4: Save to disk
	jsr enable_kernal
	load_zp $FB, highscore_file_w
	load_zp $AE, highscore_table
	lda #160
	sta temp
	jsr save_file
	jsr setup_interrupt

	jmp end

next:
	inc counter
	lda counter
	cmp #10
	beq end
	jmp loop

end:
	jsr show_highscore
	jsr wait_input
	rts
.endproc

; Scroll down the highscore table by copying bytes from the bottom, one line at a time
; Preload X with the index of the first byte to copy
.proc scroll_down
	cpx #(160 - 16)
	bne not_last_line
	rts

not_last_line:
	stx temp + 1
	ldx #(160 - 32) ; Line before the last
	ldy #(160 - 16) ; Last line
loop_line: ; copy 16 bytes from line L to line L + 1
	lda highscore_table, x
	sta highscore_table, y
	inx
	iny
	txa
	and #15 ; Check if x is a multiple of 16 (end of line)
	bne loop_line

	; Line copied, go back one line
	txa
	sec
	sbc #32
	tax
	tya
	sec
	sbc #32
	tay
	cmp temp + 1
	bne loop_line	
	
	ldx temp + 1
	rts
.endproc

.data
def_sstring str_highscore, "highscore"

highscore_file_w:
	PString "@0:highscore,s,w"

.bss

highscore_table:
	.res 160

counter:
	.res 1
