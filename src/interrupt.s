.include "interrupt.inc"
.include "util.inc"
.include "input.inc"
.include "sid.inc"

.code

.proc nmi_routine
	rti
.endproc

; Disable Kernal and setup raster interrupt
.proc setup_interrupt
	load_zp $FFFA, nmi_routine

	; Disable CIA1 timer interrupt
	lda #$7F
	sta $DC0D
	lda $DC0D ; ack any pending IRQ

	; Bank out BASIC ROM and Kernal
	lda #29
	sta 1

	; Set vblank routine
	load_zp $FFFE, vblank
	lda #0
	sta $D012
	lda $D011
	and #127
	sta $D011

	; Enable raster IRQs
	lda #$01
	sta $D01A

	ldx #1
	lda charset_timer_table, x
	sta rotation_counter
	rts
.endproc

.proc disable_raster_interrupt
	; Disable raster IRQs
	sei
	lda #$00
	sta $D01A
	cli

	rts
.endproc

; Disable raster interrupt and bank in the kernal
.proc enable_kernal
	jsr disable_raster_interrupt
	lda #30
	sta 1
	rts
.endproc

.proc vblank
	save_registers
	jsr play_music
	jsr scan_input

	; Rotate charset
	dec rotation_counter
	bne skip_rotation

	; Pick the next value for D018 from a table
	lda $D018
	and #14
	lsr
	tax
	lda charset_rotation_table, x
	sta $D018
	lda charset_timer_table, x
	sta rotation_counter

skip_rotation:
	dec $D019 ; Ack Vic II interrupt
	restore_registers
	rti
.endproc

.data
charset_rotation_table:
	.byte 0 ; unused
	.byte 4
	.byte 6
	.byte 8
	.byte 2
charset_timer_table:
	.byte 0 ; unused
	.byte 12
	.byte 8
	.byte 8
	.byte 8

.bss
rotation_counter:
	.res 1
