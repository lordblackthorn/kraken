; set_active_sprite macro must be called at least once, to set up the current active sprite.
; All the functions here will operate on that sprite
; set_sprite_x and set_sprite_y add the border offset, so position 0, 0 is the top left pixel of the visible area of the screen

.include "def.inc"
.include "math.inc"
.include "sprite.inc"
.include "util.inc"

.code

.proc enable_sprite
	lda spr_mask
	ora $D015
	sta $D015
	rts
.endproc

; Preload spr_y with the y coordinate
; X will be updated
.proc set_sprite_y
	lda spr_active
	; y coordinate is stored at $D001 + (id * 2)
	asl
	tax
	lda spr_y
	clc
	adc #50
	sta $D001, x
	rts
.endproc

; Preload spr_x with the x coordinate (16bit)
; X will be updated
.proc set_sprite_x
	; x coordinate is stored at $D000 + (id * 2)
	lda spr_active
	asl
	tax

	clc
	lda spr_x
	adc #24
	sta $D000, x
	bcs set_bit8

	lda spr_x + 1
	bne set_bit8

	; If this point is reached, then the ninth bit must be reset
	lda spr_mask
	eor #$FF
	and $D010
	sta $D010
	rts

set_bit8:
	lda spr_mask
	ora $D010
	sta $D010
	rts
.endproc

; Preload A with the color code
; Overwrite the sprite color with the one contained in A
; X will be updated
.proc set_sprite_color
	ldx spr_active
	sta $D027, x
	rts
.endproc

.data

spr_y:
	.res 1

spr_x:
	.res 2

spr_mask:
	.res 1

spr_active:
	.res 1
