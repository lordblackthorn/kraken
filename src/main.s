.include "def.inc"
.include "math.inc"
.include "input.inc"
.include "sprite.inc"
.include "string.inc"
.include "util.inc"
.include "screen.inc"
.include "disk.inc"
.include "interrupt.inc"
.include "sid.inc"
.include "prompt.inc"
.include "walker.inc"
.include "rand.inc"
.include "map.inc"
.include "ai.inc"
.include "stage.inc"
.include "statusbar.inc"
.include "item.inc"
.include "score.inc"
.include "highscore.inc"
.include "title_screen.inc"

.code

max_anim_timer = 10
max_invulnerability_timer = 100
max_algae_timer = 100

.macro load addr, file
	load_zp $FD, addr
	load_zp $FB, file
	jsr load_file_rle
.endmac

main:
	jsr load_splash

	load sprite_addr, sprite_file
	load screen_bak, map_file
	load titlescreen, titlescreen_file
	load_zp $FB, music_file
	jsr load_prg
	jsr load_charset

	load_zp $FD, highscore_table
	load_zp $FB, highscore_file
	jsr load_file

	lda #0
	jsr set_tune

	; Set the border to black
	lda #0
	sta $D020
	; Set background color
	lda #0
	sta $D021
	; Set multicolor
	lda #6
	sta $D022
	lda #3
	sta $D023
	; Enable multicolor mode
	lda $D016
	ora #16
	sta $D016

	; Switch Vic-II bank to use memory $4000-$7FFF
	lda $DD00
	and #%11111100
	ora #%00000010
	sta $DD00

	jsr init_screen

	jsr setup_interrupt	
	jsr init_meta_map

restart:
	jsr show_title_screen

	lda #1
	jsr set_tune

	lda #2
	sta lives

	lda #0
	sta curr_stage
	sta score

	jmp start

new_stage:
	lda #0
	sta $D015
	lda curr_stage
	cmp #last_stage
	bne show_score

	; Game finished
	jsr show_victory_screen
	jsr update_highscore
	jmp restart

show_score:
	jsr show_score_screen

start:
	jsr load_stage

	lda #0
	sta new_player_direction
	sta tile_handled
	sta collision

	lda #0
	sta frame_counter
	lda #max_anim_timer
	sta anim_timer

	lda #0
	sta invulnerability_timer
	sta algae_timer

	lda $D01E ; Reset collisions

main_loop:
	; Sync
	lda $D012
	cmp #255
	bne main_loop

	lda $D01E ; Always read this to make sure it gets cleared
	sta collision

	lda key_taken
	beq no_key

	; Key taken, check if the player is on the gate
	jsr check_gate
	bne no_key

	inc curr_stage
	jmp new_stage

no_key:
	lda algae_hit
	beq no_algae_just_hit
	lda #max_algae_timer
	sta algae_timer
	lda #0
	sta algae_hit

no_algae_just_hit:
	lda algae_timer
	beq no_slow_down
	dec algae_timer

no_slow_down:
	dec anim_timer
	bne skip_frame_update

	; Animation
	lda #max_anim_timer
	sta anim_timer
	; frame_counter += 1 (mod 4)
	inc frame_counter
	lda frame_counter
	and #$03
	sta frame_counter

skip_frame_update:
	jsr animate

	lda invulnerability_timer
	bne invulnerable

	; Check for collisions
	lda collision
	and #1
	beq handle_input; No Collision

	; Collision!
	jsr play_hit_sound
	dec lives
	bne collided
	jmp gameover

collided:
	jsr update_statusbar
	lda #max_invulnerability_timer
	sta invulnerability_timer

invulnerable:
	dec invulnerability_timer
	lda $D015
	eor #1
	sta $D015

handle_input:
	lda input_method
	beq use_keyboard

	lda joy_status
	lsr
	bcc move_up
	lsr
	bcc move_down
	lsr
	bcc move_left
	lsr
	bcc move_right
	jmp continue

use_keyboard:
	lda key_pressed
	cmp #k_I
	beq move_up
	cmp #k_J
	beq move_left
	cmp #k_L
	beq move_right
	cmp #k_K
	beq move_down

continue:
	jsr update_walkers
	lda algae_timer
	and #1
	beq handle_player
	jmp main_loop

handle_player:
	load_zp $61, walkers
	jsr is_walker_snapped
	beq snapped

	lda #0
	sta tile_handled

	jsr move_walker
	jmp main_loop

snapped:
	jsr calc_walker_offset

	lda tile_handled
	bne tile_already_handled

	lda #1
	sta tile_handled

	jsr update_floor
	jsr check_blocks

tile_already_handled:
	jsr handle_direction_change
	jmp main_loop

move_up:
	lda #0
	sta new_player_direction
	jmp continue

move_right:
	lda #1
	sta new_player_direction
	jmp continue

move_down:
	lda #2
	sta new_player_direction
	jmp continue

move_left:
	lda #3
	sta new_player_direction
	jmp continue

gameover:
	lda #0
	sta $D015
	jsr show_gameover_screen
	jsr update_highscore
	jmp restart

.proc handle_direction_change
	; Check for going back
	ldy #Walker::direction
	lda ($61), y
	clc
	adc #2
	and #$F3
	cmp new_player_direction
	beq valid_direction

	load_zp $FB, $400
	inc16 $FB, walker_offset
	ldy #1
	lda ($FB), y
	bne in_fork

	jsr move_walker
	rts

in_fork:
	; If in a fork load the fork data in $FD/$FE and the fork routine in $6F
	sta $70
	dey
	lda ($FB), y
	sta $6F

	ldy #40
	lda ($FB), y
	sta $FD
	iny
	lda ($FB), y
	sta $FE
	ldy #Walker::direction
	lda walkers, y
	asl
	inc16 $FD
	jsr callback
	rts

valid_direction:
	ldy #Walker::direction
	lda new_player_direction
	sta walkers, y

	jsr move_walker
	rts
.endproc

.proc animate
	.repeat max_walkers, i
	ldy #Walker::direction
	lda walkers + .sizeof(Walker) * i, y
	asl
	asl
	clc
	adc frame_counter
	adc #sprite_ptr_base
	.if i = 0
	adc #player_sprite
	.elseif i = 4 || i = 5
	adc #fast_kraken_sprite
	.endif
	ldx #i ; sprite_id
	sta screen + 1016, x
	.endrep

	rts
.endproc

.proc update_floor
	load_zp $FB, screen
	inc16 $FB, walker_offset
	load_zp $FD, $D800
	inc16 $FD, walker_offset
	ldy #0
	jsr blit_bubbles
	rts
.endproc

.proc load_splash
	; Clear the screen
	load_zp $FD, $400
	lda #32
	ldx #4
	jsr memfill

	load_zp $FB, splash_file
	jsr open_file_r	
	load_zp $61, $400
	load_zp $63, $D800
loop:
	load_zp $FD, splash_buff
	lda #2
	sta temp
	lda #0
	sta temp + 1
	jsr read_file_chunk

	ldy #0
	lda splash_buff
	sta ($61), y	
	lda splash_buff + 1
	sta ($63), y	
	inc16_by_1 $61
	inc16_by_1 $63
	cmp_eq16_value $61, 2024
	bne loop

	jsr close_file
	rts
.endproc

; Check if the player is on the gate
; Set the Zero flag if the player is on the gate
.proc check_gate
	ldy #Walker::y_p
	lda walkers, y
	cmp stage_gate_y
	bne end

	ldy #Walker::x_p
	lda walkers, y
	sta temp
	iny
	lda walkers, y
	sta temp + 1
	cmp_eq16 temp, stage_gate_x

end:
	rts
.endproc

.proc load_charset
	load charset_addr, charset_file

	; Duplicate the charset
	load_zp $FB, charset_addr
	load_zp $FD, charset2_addr
	ldx #8
	jsr memcopy
	load_zp $FB, charset_addr
	load_zp $FD, charset3_addr
	ldx #8
	jsr memcopy
	load_zp $FB, charset_addr
	load_zp $FD, charset4_addr
	ldx #8
	jsr memcopy

	; Replace parts of charsets 2-4 with new content load from file
	load charset2_addr, charset2_file
	load charset3_addr, charset3_file
	load charset4_addr, charset4_file

	rts
.endproc

.data

charset_file:
	PString "charset,s"
charset2_file:
	PString "charset2,s"
charset3_file:
	PString "charset3,s"
charset4_file:
	PString "charset4,s"
sprite_file:
	PString "sprite,s"
map_file:
	PString "map,s"
splash_file:
	PString "splash,s"
music_file:
	PString "music"
highscore_file:
	PString "highscore,s"
titlescreen_file:
	PString "title,s"

.bss
tile_handled:
	.res 1
frame_counter:
	.res 1
anim_timer:
	.res 1
invulnerability_timer:
	.res 1
collision:
	.res 1
splash_buff:
	.res 2
algae_timer:
	.res 1
