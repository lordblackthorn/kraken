.include "title_screen.inc"
.include "util.inc"
.include "def.inc"
.include "input.inc"
.include "sid.inc"
.include "highscore.inc"
.include "screen.inc"

.code

.proc highlight_input_method
	lda input_method
	beq highlight_keyboard

	lda #5
	sta $D800 + 664
	sta $D800 + 665
	sta $D800 + 666
	lda #7
	sta $D800 + 668
	sta $D800 + 669
	sta $D800 + 670
	rts

highlight_keyboard:
	lda #7
	sta $D800 + 664
	sta $D800 + 665
	sta $D800 + 666
	lda #5
	sta $D800 + 668
	sta $D800 + 669
	sta $D800 + 670
	rts
.endproc

.proc show_title_screen
	lda #2
	jsr set_tune

reset_title_screen:
	load_zp $FB, titlescreen
	load_zp $FD, screen
	ldx #4
	jsr memcopy
	load_zp $FB, titlecolor
	load_zp $FD, $D800
	ldx #4
	jsr memcopy

	jsr get_last_key
	jsr get_fire_key
wait_key:
	jsr highlight_input_method

	jsr get_fire_key
	cmp #1
	beq end
	jsr get_last_key
	cmp #k_F1
	beq f1_pressed
	cmp #k_F3
	beq f3_pressed
	cmp #k_Space
	bne wait_key

end:
	; Space/Fire pressed, start the game
	rts

f3_pressed:
	jsr clear_text_screen
	jsr show_highscore
	jsr wait_input
	jmp reset_title_screen

f1_pressed:
	lda input_method
	eor #1
	sta input_method
	lda #$FF
	sta joy_status
	jmp wait_key

	rts
.endproc

.data

input_method:
	.byte 0
