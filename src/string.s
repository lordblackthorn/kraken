.code

.include "math.inc"
.include "util.inc"
.include "string.inc"

ch_space = 32
ch_0 = 48

; Print a string on the screen
; Preload FB for screen address and $65 with the string address
; preload 'temp' with the minimum number of characters of the string to print; if
; the characters in the string are fewer than 'temp', spaces will be added at the end
; After the call, Y contains the number of characters printed
.proc print_string
	push16 $65
	ldy #0
	lda ($65), y
	sta temp + 1
	inc16_by_1 $65
loop:
	cpy temp + 1
	beq fill
	lda ($65), y
	sta ($FB), y
	iny
	jmp loop

fill:
	; Fill up the remaining characters with spaces
	cpy temp 
	bcs end
	lda #ch_space
fill_loop:
	sta ($FB), y
	iny
	cpy temp
	bne fill_loop

end:
	pop16 $65
	rts
.endproc

; Clear space by printing as many spaces as specified in 'temp'
; Preload $FB with the screen position
.proc clear_text
	ldy #0
	lda #ch_space
loop:	
	sta ($FB), y
	iny
	cpy temp
	bne loop

	rts
.endproc

; Print the digit contained in A on the screen
; Use FB for the screen position
.proc print_digit
	clc
	adc #ch_0
	sta ($FB), y
	iny
	rts
.endproc

; Print a number on the screen
; Use FB for screen
; Preload 'bcd' with the number to print
; After the call, Y contains the number of characters printed
.proc print_byte
	ldy #0

	lda bcd + 1
	beq second_digit
	jsr print_digit	

second_digit: ; bcd higher nybble
	lda bcd
	lsr
	lsr
	lsr
	lsr
	beq check_leading_zero

print_second_digit:
	jsr print_digit	

third_digit:
	lda bcd
	and #$0F
	jsr print_digit	

	rts

check_leading_zero:
	cpy #0
	beq third_digit
	jmp print_second_digit

.endproc

