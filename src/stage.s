.include "math.inc"
.include "util.inc"
.include "walker.inc"
.include "stage.inc"
.include "sprite.inc"
.include "def.inc"
.include "statusbar.inc"
.include "rand.inc"

.code

walker_data = temp + 2

; Load a stage.
; Preload curr_stage with the stage number to load
.proc load_stage
	lda #0
	sta key_taken
	sta algae_hit
	sta seed
	sta num_pearls

	; Disable sprites
	lda #0
	sta $D015

	; Copy back a fresh map into the screen
	load_zp $FB, screen_bak
	load_zp $FD, screen
	ldx #4
	jsr memcopy

	; Reset colors
	load_zp $FD, $D800
	lda #12
	ldx #4
	jsr memfill
	lda #3 ; Status bar color
	ldx #0
loop:
	sta $D800 + 960, x
	inx
	cpx #40
	bne loop

	jsr update_statusbar

	; Setup $FB to point to the stage data
	load_zp $FB, stages
	lda curr_stage
	sta temp
	lda #0
	sta temp + 1
	.repeat 5
	shl16 temp
	.endrep
	inc16 $FB, temp

	ldy #26
	lda ($FB), y
	sta stage_gate_x
	iny
	lda ($FB), y
	sta stage_gate_x + 1
	iny
	lda ($FB), y
	sta stage_gate_y

	jsr init_walkers
	jsr init_blocks

	rts
.endproc

.macro init_block addr
	lda #0
	sta addr + $400 + 122
	lda ($FB), y
	sta addr + $400 + 123
	iny
.endmacro

.proc init_blocks
	ldy #6
	init_block 0
	init_block 8
	init_block 16
	init_block 22
	init_block 30
	init_block 240
	init_block 248
	init_block 256
	init_block 262
	init_block 270
	init_block 480
	init_block 488
	init_block 496
	init_block 502
	init_block 510
	init_block 720
	init_block 728
	init_block 736
	init_block 742
	init_block 750
	rts
.endproc

.proc init_walker
	lda #0
	ldy #Walker::direction
	sta ($61), y

	; Get position.
	; We "cancel" the two LSB and we end up with position multiplied by 4,
	; which is what we need to access the position vector
	stx temp
	lda walker_data
	and #252
	tax
	lda pos_vector, x
	ldy #Walker::x_p
	sta ($61), y
	sta spr_x
	inx
	iny
	lda pos_vector, x
	sta ($61), y
	sta spr_x + 1
	inx
	lda pos_vector, x
	ldy #Walker::y_p
	sta ($61), y
	sta spr_y
	ldx temp

	txa
	ldy #Walker::id
	sta ($61), y
	sta spr_active

	stx temp
	lda #1
	shl
	ldy #Walker::sprite_mask
	sta ($61), y
	sta spr_mask
	jsr set_sprite_x
	jsr set_sprite_y
	ldx temp

	lda #1
	ldy #Walker::speed
	sta ($61), y

	txa
	beq init_player ; If a = 0, init player
	cmp #4
	bcc init_slow_kraken

	; Init fast kracken
	lda #15
	jsr set_sprite_color
	jmp end

init_player:
	lda #2
	ldy #Walker::speed
	sta ($61), y
	lda #10
	jsr set_sprite_color
	jmp end

init_slow_kraken:
	lda #3
	jsr set_sprite_color

end:
	jsr enable_sprite
	rts
.endproc

; Initialize walkers data
; Preload $FB with stage data
; Use temp + 2 to store walker data
.proc init_walkers
	load_zp $61, walkers
	ldx #0

loop:
	txa
	tay
	lda ($FB), y
	sta walker_data
	ldy #Walker::active
	and #1
	sta ($61), y
	beq next

	jsr init_walker

next:
	lda #.sizeof(Walker)
	inc16 $61
	inx
	cmp_eq16_value $61, walkers_end
	bne loop

	rts
.endproc


.data

; Stage data
; Format:
; byte 0 - 5: Walkers data. Each walker is 1 byte
;             Bit 0 - Active
;             Bit 1 - Unused
;             Bit 2-5 - Position (used as index in the pos vector, see below)
;             The first walker is the player, walkers 1,2,3 are slow krakens, walkers 4 and 5 are fast
; byte 6 - 25: Content of the blocks in the map, starting from the block on the 
;              top left and continuing horizontally and then vertically
;              Values: 0 - Pearl
;                      1 - Key
;                      2 - Algae
;                      3 - Heart
; byte 26-27: X Position of the gate (in pixels)
; byte 28: Y position of the gate (in pixels)
; byte 29 - 31: padding

stages:
	; Stage 0
	.byte 1, 5, 0, 0, 0, 0
	.byte 1, 3, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.byte 2, 2, 2, 2, 2
	.word 0
	.byte 0
	.byte 0, 0, 0
	; Stage 1
	.byte 1, 5, 9, 0, 0, 0
	.byte 2, 2, 2, 2, 2
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 1
	.word 144
	.byte 96
	.byte 0, 0, 0
	; Stage 2
	.byte 1, 0, 0, 0, 5, 0
	.byte 2, 0, 0, 0, 2
	.byte 0, 0, 0, 0, 1
	.byte 0, 0, 0, 0, 0
	.byte 2, 0, 2, 0, 2
	.word 144
	.byte 96
	.byte 0, 0, 0
	; Stage 3
	.byte 1, 9, 0, 0, 5, 0
	.byte 0, 2, 0, 2, 0
	.byte 0, 0, 1, 0, 0
	.byte 0, 2, 0, 2, 0
	.byte 0, 0, 2, 0, 0
	.word 144
	.byte 96
	.byte 0, 0, 0
	; Stage 4
	.byte 13, 5, 0, 0, 9, 0
	.byte 3, 0, 0, 2, 2
	.byte 0, 0, 0, 2, 2
	.byte 0, 2, 0, 0, 0
	.byte 1, 0, 0, 0, 0
	.word 304
	.byte 0
	.byte 0, 0, 0
	; Stage 5
	.byte 13, 5, 0, 0, 9, 0
	.byte 0, 0, 2, 2, 1
	.byte 0, 0, 2, 2, 2
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.word 304
	.byte 0
	.byte 0, 0, 0
	; Stage 6
	.byte 13, 5, 1, 0, 9, 0
	.byte 0, 0, 2, 0, 1
	.byte 0, 0, 2, 0, 2
	.byte 0, 0, 2, 0, 0
	.byte 0, 0, 2, 0, 0
	.word 304
	.byte 0
	.byte 0, 0, 0
	; Stage 7
	.byte 13, 5, 1, 0, 9, 0
	.byte 0, 0, 2, 0, 1
	.byte 0, 0, 2, 0, 2
	.byte 0, 0, 2, 0, 0
	.byte 0, 0, 2, 0, 0
	.word 304
	.byte 0
	.byte 0, 0, 0
	; Stage 8
	.byte 9, 5, 1, 17, 13, 0
	.byte 0, 0, 0, 0, 0
	.byte 1, 2, 2, 2, 2
	.byte 0, 0, 2, 0, 0
	.byte 0, 0, 0, 0, 0
	.word 0
	.byte 176
	.byte 0, 0, 0
	; Stage 9
	.byte 9, 5, 1, 17, 13, 0
	.byte 0, 0, 2, 0, 0
	.byte 0, 2, 1, 2, 0
	.byte 0, 3, 2, 0, 0
	.byte 0, 0, 2, 0, 0
	.word 0
	.byte 176
	.byte 0, 0, 0
	; Stage 10
	.byte 9, 5, 1, 17, 13, 0
	.byte 0, 2, 1, 2, 0
	.byte 0, 2, 2, 2, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.word 0
	.byte 176
	.byte 0, 0, 0
	; Stage 11
	.byte 9, 5, 1, 17, 13, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 2, 2, 2, 0
	.byte 0, 2, 1, 2, 0
	.word 0
	.byte 176
	.byte 0, 0, 0
	; Stage 12
	.byte 17, 5, 0, 9, 13, 1
	.byte 0, 2, 0, 0, 0
	.byte 0, 1, 0, 0, 0
	.byte 0, 2, 0, 2, 0
	.byte 0, 2, 0, 2, 0
	.word 304
	.byte 96
	.byte 0, 0, 0
	; Stage 13
	.byte 17, 5, 0, 9, 13, 1
	.byte 0, 2, 0, 0, 0
	.byte 0, 0, 0, 0, 0
	.byte 0, 2, 0, 2, 2
	.byte 0, 0, 0, 2, 1
	.word 0
	.byte 0
	.byte 0, 0, 0
	; Stage 14
	.byte 17, 5, 0, 9, 13, 1
	.byte 0, 2, 0, 0, 0
	.byte 0, 0, 2, 0, 0
	.byte 0, 2, 3, 2, 0
	.byte 0, 0, 1, 2, 0
	.word 0
	.byte 0
	.byte 0, 0, 0
	; Stage 15
	.byte 17, 5, 21, 9, 13, 1
	.byte 0, 2, 0, 2, 1
	.byte 0, 0, 2, 0, 0
	.byte 0, 2, 0, 2, 0
	.byte 0, 0, 0, 0, 0
	.word 0
	.byte 176
	.byte 0, 0, 0

; Position vector. Forks positions only. Values in pixel
; byte 0 - 1: x
; byte 2: y
; byte 3: padding
pos_vector:
; pos 0 - Top left corner
.word 0
.byte 0
.byte 0
; pos 1 - Bottom right corner
.word 304
.byte 176
.byte 0
; pos 2 - Top right corner
.word 304
.byte 0
.byte 0
; pos 3 - Bottom left corner
.word 0
.byte 176
.byte 0
; pos 4 - Center like
.word 128
.byte 96
.byte 0
; pos 5 - Center right
.word 304
.byte 96
.byte 0

.bss

curr_stage:
	.res 1

key_taken:
	.res 1

algae_hit:
	.res 1

num_pearls:
	.res 1

stage_gate_x:
	.res 2

stage_gate_y:
	.res 1
