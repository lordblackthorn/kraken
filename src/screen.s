.include "screen.inc"
.include "math.inc"
.include "def.inc"
.include "util.inc"
.include "interrupt.inc"

.code

D018_char = (((screen - vicii) / 1024) << 4) + ((charset_addr - vicii) / 1024)

.proc init_screen
	lda #D018_char
	sta $D018
	rts
.endproc

.proc clear_text_screen
	; Clear the screen
	load_zp $FD, screen
	lda #32
	ldx #4
	jsr memfill
	; Set the color
	load_zp $FD, $D800
	lda #1
	ldx #4
	jsr memfill
	rts
.endproc

